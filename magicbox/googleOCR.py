import base64
import os
import re
import sys
from google.cloud import vision
from google.cloud.vision import types
from gcloud import storage
import cv2


storage_client = storage.Client()
bucket = storage_client.get_bucket('magic_box_images')
blob = bucket.blob('forOCR.jpg')

with open('de.jpg', 'rb') as photo:
    blob.upload_from_file(photo)
image_uri = 'gs://magic_box_images/forOCR.jpg'
client = vision.ImageAnnotatorClient()
image = types.Image()
image.source.image_uri = image_uri

response = client.text_detection(image=image)

prob_name_list = []
prob_last_list = []
word_list = response.text_annotations[1:]
text = response.text_annotations[0].description
i = 0
while i < len(word_list) -1:
    if word_list[i].description.isupper():
        if word_list[i+1].description.isupper():
            prob_name_list.append(word_list[i].description)
            prob_last_list.append(word_list[i+1].description)
            i = i + 1
        else:
            i = i + 2
    else:
        i = i+1

print(text,prob_name_list, prob_last_list)