#!/usr/bin/env python
import PySimpleGUI as sg
# import PySimpleGUIQt as sg
import cv2
import numpy as np
import sys
from sys import exit as exit

"""
Demo program that displays a webcam using OpenCV
"""
def main():

    sg.ChangeLookAndFeel('Black')

    # define the window layout
    layout = [[sg.Text('OpenCV Demo', size=(40, 1), justification='center', font='Helvetica 20')],
              [sg.Image(filename='', key='image')],
              [sg.Button('Record', size=(10, 1), font='Helvetica 14'),
               sg.Button('Stop', size=(10, 1), font='Any 14'),
              sg.Button('Exit', size=(10, 1), font='Helvetica 14'),]]

    # create the window and show it without the plot
    window = sg.Window('Demo Application - OpenCV Integration', layout,
                       location=(800,400))

    # ---===--- Event LOOP Read and display frames, operate the GUI --- #
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FOURCC,cv2.VideoWriter_fourcc(*'MJPG')) #cv2.VideoWriter_fourcc('M','J','P','G'))#cv2.VideoWriter_fourcc(*'MJPG'))
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
    cap.set(cv2.CAP_PROP_BUFFERSIZE, 1)
    recording = False
    while True:
        event, values = window.Read(timeout=20)
        if event == 'Exit' or event is None:
            sys.exit(0)
        elif event == 'Record':
            recording = True
        elif event == 'Stop':
            recording = False
            img = np.full((480, 640),255)
            imgbytes=cv2.imencode('.png', img)[1].tobytes() #this is faster, shorter and needs less includes
            window.FindElement('image').Update(data=imgbytes)

        if recording:
            ret, frame = cap.read()
            center_img = frame[20 : 2155, 390 : 3590]
            text = "Bluryness"
            fm = cv2.Laplacian(center_img, cv2.CV_64F).var()
            cv2.putText( frame, "{}: {:.2f}".format(text, fm), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
            box = np.array([[390, 20], [3590, 20],[3590, 2155],[390, 2155]])
            contour_img = cv2.drawContours(frame.copy(),[box],0,(0,0,255),3)
            smaller_img = cv2.resize( contour_img,(1280, 720))       
            imgbytes=cv2.imencode('.png', smaller_img.astype(np.uint8))[1].tobytes()
            window.FindElement('image').Update(data=imgbytes)

main()
exit()
