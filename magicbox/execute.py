import pyrealsense2 as rs
import numpy as np
import cv2
import time 
import timeit
from google.cloud import vision
from google.cloud.vision import types
from gcloud import storage
from matplotlib import pyplot as plt
import Levenshtein
import csv
import os
from threading import Thread
import datetime
from pyzbar import pyzbar
os.system("export GOOGLE_APPLICATION_CREDENTIALS='/home/raiymbek/key.json'")

class Execute:
	def __init__(self):
		self.configure_streams()
		self.totalHeight = self.depth_now()
		self.get_all_user_list()
		print(self.totalHeight) 


	def depth_now(self):
		depth = 0
		i = 0
		while i < 5:
			frames = self.pipeline.wait_for_frames()
			depth_frame = frames.get_depth_frame()
			if 0 >= depth_frame.get_distance(228, 140):
				continue
			depth += depth_frame.get_distance(228, 140)
			i+=1
		return depth / 5

	def configure_streams(self):
		self.pipeline = rs.pipeline()
		config = rs.config()
		config.enable_stream(rs.stream.depth)
		self.pipeline.start(config)
		"""
		self.vs = WebcamVideoStream(src=0).start()
		"""
		self.vs = cv2.VideoCapture(0, cv2.CAP_V4L2)
		self.vs.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M','J','P','G')) #cv2.VideoWriter_fourcc(*'MJPG'))
		self.vs.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)
		self.vs.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
		self.vs.set(cv2.CAP_PROP_BUFFERSIZE, 1);
		#ret, frame = self.vs.read()
		

	def detect_package(self):
		current = self.totalHeight
		i = 0
		while True:
			print(current)
			frames = self.pipeline.wait_for_frames()
			depth_frame = frames.get_depth_frame()
			depth = depth_frame.get_distance(228, 140)
			if depth != 0:
				if abs(current - depth) > 0.01:
					i = 0
					current = depth 
				else:
					i+=1


			if i == 50 and current != self.totalHeight:
				self.pipeline.stop()
				break
					
			key = cv2.waitKey(1)
			# Press esc or 'q' to close the image window
			if key & 0xFF == ord('q') or key == 27:
				cv2.destroyAllWindows()
				break

			if self.totalHeight - current > 1:
				self.limit = 300
			else:
				self.limit = 300

		return self.totalHeight - current, current

	def get_img(self, img_path):
		img = cv2.imread(img_path)
		img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
		#img = cv2.resize(img, (480, 752))
		return img

	# just prints an image
	def print_img(self, img):
		plt.imshow(img, cmap='Greys_r')
		plt.show()

	def get_box_img(self, img_path = "picture.jpg"):
		img = self.get_img(img_path)
		smaller_img = cv2.resize(img, (int(img.shape[1]/8), int(img.shape[0]/8)))
		blur = cv2.GaussianBlur(smaller_img,(11,11),0)                    
		lap = cv2.Laplacian(blur, cv2.CV_32F)                                
		grey = (cv2.cvtColor(lap, cv2.COLOR_BGR2GRAY) + 120).astype(np.uint8)
		edges = cv2.Canny(grey, 50, 150, apertureSize = 5)
		#edges = hist_eq(edges)
		contours, hierarchy = cv2.findContours(edges, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
		contour_img = img.copy()
		#print_img(cv2.drawContours(contour_img,[contours],-1,(0,0,255),3))
		finalContour = np.array([[[0,0]]])
		for cnt in contours:
			cnt = cv2.convexHull(cnt)
			if cv2.contourArea(cnt) > 500:
				finalContour = np.vstack((finalContour, cnt))
		finalContour = cv2.convexHull(finalContour[1:])
		rect = cv2.minAreaRect(finalContour)
		box = cv2.boxPoints(rect)
		box = np.int0(box)
		box = 8 * box
		contour_img = cv2.drawContours(contour_img,[box],0,(0,0,255),3)
		return contour_img, box

	def get_google_text(self, img):
		storage_client = storage.Client()
		bucket = storage_client.get_bucket('magic_box_images')
		blob = bucket.blob('forOCR.jpg')

		with open('de.jpg', 'rb') as photo:
			blob.upload_from_file(photo)

		image_uri = 'gs://magic_box_images/forOCR.jpg'
		client = vision.ImageAnnotatorClient()
		image = types.Image()
		image.source.image_uri = image_uri

		response = client.text_detection(image=image)

		prob_name_list = []
		prob_last_list = []
		word_list = response.text_annotations[1:]
		text = response.text_annotations[0].description
		i = 0
		while i < len(word_list) -1:
			if word_list[i].description.isupper():
				if word_list[i+1].description.isupper():
					prob_name_list.append(word_list[i].description)
					prob_last_list.append(word_list[i+1].description)
					i = i + 1
				else:
					i = i + 2
			else:
				i = i+1
		return text,prob_name_list, prob_last_list

	def get_tesseract_text(self, img):
		pass

	def get_package(self, img, box, current):
		box = np.float32(box)
		length, width = self.get_size(box)
		print("Pixel size", length, width)
		cm_length = (current / self.totalHeight) * (28.5 / 1280) * length
		cm_width = (current / self.totalHeight) * (30 / 1280) * width
		next_box = np.float32([[0, length], [0,0], [width,0], [width, length]])
		M = cv2.getPerspectiveTransform(box, next_box)
		return cv2.warpPerspective(img, M, (width, length)), cm_length, cm_width

	def get_size(self, box):
		first, second, third, fourth = box
		length = int(( (first[0] - second[0])**2 + (first[1] - second[1])**2 )**0.5)
		width = int(( (third[0] - second[0])**2 + (third[1] - second[1])**2 )**0.5)
		return length, width

	def get_img_th(self):
		#vs = WebcamVideoStream(src=0).start()

		while True:
			# grab the frame from the threaded video stream and resize it
			# to have a maximum width of 400 pixels
			ret, frame = self.vs.read()
			center_img = frame[5 : 2155, 220 : 3440]
			focus = cv2.Laplacian(center_img, cv2.CV_64F).var()
			print(focus)
			if focus > 50:
				#self.vs.stop()
				self.vs.release()
				break
			#self.print_img(center_img)
		return center_img

	def get_all_user_list(self):
		self.name_list = []
		self.last_list = []
		with open("UserFirstandLastNames.csv") as csv_file:
			csv_reader = csv.reader(csv_file, delimiter=',')
			for row in csv_reader:
				self.name_list.append(row[0])
				self.last_list.append(row[1])

	def get_recipient_name(self,prob_name_list, prob_last_list):
		recipient = []
		for name_ind in range(len(self.name_list)):
			for prob_name_ind in range(len(prob_name_list)):
				if self.name_list[name_ind] != "":
					if Levenshtein.ratio(self.name_list[name_ind].upper(), prob_name_list[prob_name_ind]) >= 0.85 - (1 / max(len(self.name_list[name_ind]),len(prob_name_list[prob_name_ind]))):
						if Levenshtein.ratio(self.last_list[name_ind].upper(), prob_last_list[prob_name_ind]) >= 0.85 - (1 / max(len(self.last_list[name_ind]),len(prob_last_list[prob_name_ind]))):
							toAdd = self.name_list[name_ind] + " " + self.last_list[name_ind]
							if not(toAdd in recipient):
								recipient.append(toAdd) 
		if len(recipient) == 1:
			print("The recipient is " + recipient[0])
			return True, recipient[0]
		else:
			print("Possible recipients are: ")
			print(recipient)
			return False, recipient 

	def get_barcode(self, package):
		barcodes = pyzbar.decode(package)
		return barcodes

	def execute(self):
		print("starting")
		boxHeight, current = self.detect_package()
		start = timeit.default_timer()
		image = self.get_img_th()
		#self.print_img(image)
		stop = timeit.default_timer()
		print('Time to get the image: ', stop - start)
		cv2.imwrite("picture.jpg", image)
		contour_img, box = self.get_box_img()
		stop1 = timeit.default_timer()
		print('Time to get dimentions(locker opens): ', stop1 - stop)
		package, cm_length, cm_width = self.get_package(image, box, current)
		print("Dimentions: ", boxHeight, cm_length, cm_width)
		#package = cv2.cvtColor(package, cv2.COLOR_BGR2GRAY)
		cv2.imwrite("de.jpg", package)
		text, prob_name_list, prob_last_list = self.get_google_text(package)
		stop2 = timeit.default_timer()
		print('Time: send and receive the text', stop2 - stop1)
		one_rec, recipient = self.get_recipient_name(prob_name_list, prob_last_list)
		self.print_img(contour_img)
		cv2.imwrite("forOCR.jpg", contour_img)
		now = datetime.datetime.now()
		string_now = now.strftime("%Y-%m-%d %H:%M")
		cv2.imwrite("packages/" + string_now + ".jpg", package)
		file1 = open("text/" + string_now +".txt","w")
		file1.write(text)
		file1.close()
		
		cv2.destroyAllWindows()

		return boxHeight, cm_length, cm_width

class WebcamVideoStream:
	def __init__(self, src=0):
		# initialize the video camera stream and read the first frame
		# from the stream
		self.stream = cv2.VideoCapture(2, cv2.CAP_V4L2)
		#self.stream.set(cv2.CAP_PROP_AUTOFOCUS, 1)
		self.stream.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
		self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)
		self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
		while True:
			try:
				(self.grabbed, self.frame) = self.stream.read()
				break
			except:
				continue

		# initialize the variable used to indicate if the thread should
		# be stopped
		self.stopped = False

	def start(self):
		# start the thread to read frames from the video stream
		Thread(target=self.update, args=()).start()
		return self

	def update(self):
		# keep looping infinitely until the thread is stopped
		while True:
			# if the thread indicator variable is set, stop the thread
			if self.stopped:
				return

			while True:
				try:
					(self.grabbed, self.frame) = self.stream.read()
					break
				except:
					continue

	def read(self):
		# return the frame most recently read
		return self.frame

	def stop(self):
		# indicate that the thread should be stopped
		self.stopped = True

#de = Execute()
#de.execute()
