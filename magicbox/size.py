from matplotlib import pyplot as plt
import cv2
import numpy as np

def get_img(img_path):
    img = cv2.imread(img_path)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    #img = cv2.resize(img, (480, 752))
    return img

# just prints an image
def print_img(img):
    plt.imshow(img, cmap='Greys_r')
    plt.show()

img_path = "picture.jpg"
img = get_img(img_path)
#img = img[470: 2030, 460:2770]
smaller_img = cv2.resize(img, (int(img.shape[1]/8), int(img.shape[0]/8)))
print_img(smaller_img)
blur = cv2.GaussianBlur(smaller_img,(11,11),0)                    
lap = cv2.Laplacian(blur, cv2.CV_32F)                                
grey = (cv2.cvtColor(lap, cv2.COLOR_BGR2GRAY) + 120).astype(np.uint8)
edges = cv2.Canny(grey, 50, 150, apertureSize = 5)
#edges = hist_eq(edges)
image, contours, hierarchy = cv2.findContours(edges, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
contour_img = img.copy()
finalContour = np.array([[[0,0]]])
for cnt in contours:
  #cnt = cv2.convexHull(cnt)
  if cv2.contourArea(cnt) > 500:
    finalContour = np.vstack((finalContour, cnt))
finalContour = cv2.convexHull(finalContour[1:])
rect = cv2.minAreaRect(finalContour)
box = cv2.boxPoints(rect)
box = np.int0(box)
box = 8 * box
contour_img = cv2.drawContours(contour_img,[box],0,(0,0,255),3)
print_img(contour_img)