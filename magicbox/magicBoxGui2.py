import pyrealsense2 as rs
import numpy as np
import cv2
import time 
import timeit
from google.cloud import vision
from google.cloud.vision import types
from gcloud import storage
from matplotlib import pyplot as plt
import Levenshtein
import csv
import os
from threading import Thread
import datetime
import PySimpleGUI as sg
from GuiClass import Execute


col = [[sg.Text('', size=(130, 720), key='result')]]
layout = [[sg.Image(filename='smiota-blue-logo-header.png', key='logo')],
          [sg.Text('', size=(100,1), key='-OUTPUT-')],
          [sg.Button('Execute'), sg.Button('Stop Session'), sg.Button('Exit'), sg.Button('Info')],
          [sg.Button('Show'), sg.Button('Stop Preview'), sg.Button('Calibrate camera'), sg.Button('Calibrate depth')],
          [sg.Image(filename='', key='image'), sg.Column(col, scrollable = True, vertical_scroll_only = True)]]

window = sg.Window('magicBox', layout, resizable=True, size = (1280,800)).Finalize()
de = Execute(window)
toShow = False
toContinue = False
toCalibrate =False
empty_img = np.full((500, 800), 0)
empty_imgbytes=cv2.imencode('.png', empty_img)[1].tobytes()
window.FindElement('image').Update(data=empty_imgbytes)

while True:  # Event Loop

    # Getting the event
    event, values = window.Read(timeout=20)
    print(event, values)

    # If event is exiting stop the app
    if event is None or event == 'Exit':
        break

    # If SHOW then show the preview
    elif event == 'Show':
        toContinue = False
        toShow = True

    # Stopping the preview
    elif event == 'Stop Preview':
        toContinue = False
        toShow = False
        window.FindElement('image').Update(data=empty_imgbytes)

    #Start execution
    elif event == 'Execute':
        toShow = False
        toContinue = True

    # Stopping the session
    elif event == 'Stop Session':
        window['-OUTPUT-'].Update("Done")
        toContinue = False

    # camera calibration
    elif event == 'Calibrate camera':
        toContinue = False
        toCalibrate = True

    # Getting some info
    elif event == 'Info':
        popupMessage = "Smiota MagicBox \n push Calibrate button to calibrate the focus of the camera \n push Execute start scanning packages"
        sg.popup("Info", popupMessage)

    # Calibrating depth
    elif event == 'Calibrate depth':
        de.calibrate()

    else:
        empty_img = np.full((500, 800), 0)
        #empty_imgbytes=cv2.imencode('.png', empty_img)[1].tobytes()
        #window.FindElement('image').Update(data=empty_imgbytes)

    # configuring the preview
    if toShow:
        fm = de.show_preview()
        window['-OUTPUT-'].Update(str(fm))

    # configuring session
    if toContinue:
        toContinue = de.execute()
        print(toContinue)

    # configuring calibration
    if toCalibrate:
        print("sffrff")
        fm = de.show_preview()
        if fm < 100:
            window['-OUTPUT-'].Update("Clariness - " + str(fm) + ". Turn focus gently untill it reaches 200")
        elif fm > 200:
            window['-OUTPUT-'].Update("Calibrated")
            toCalibrate =False


window.Close()
de.pipeline.stop()
de.vs.release()