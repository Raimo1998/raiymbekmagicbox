# import the necessary packages
import datetime
from threading import Thread
import cv2

class FPS:
	def __init__(self):
		# store the start time, end time, and total number of frames
		# that were examined between the start and end intervals
		self._start = None
		self._end = None
		self._numFrames = 0
 
	def start(self):
		# start the timer
		self._start = datetime.datetime.now()
		return self
 
	def stop(self):
		# stop the timer
		self._end = datetime.datetime.now()
 
	def update(self):
		# increment the total number of frames examined during the
		# start and end intervals
		self._numFrames += 1
 
	def elapsed(self):
		# return the total number of seconds between the start and
		# end interval
		return (self._end - self._start).total_seconds()
 
	def fps(self):
		# compute the (approximate) frames per second
		return self._numFrames / self.elapsed()
 

class WebcamVideoStream:
	def __init__(self, src=0):
		# initialize the video camera stream and read the first frame
		# from the stream
		self.stream = cv2.VideoCapture(0, cv2.CAP_V4L2)
		self.stream.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
		#self.stream.set(cv2.CAP_PROP_AUTOFOCUS, 1)
		self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, 4280)
		self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, 3120)
		while True:
			try:
				(self.grabbed, self.frame) = self.stream.read()
				break
			except:
				continue

		# initialize the variable used to indicate if the thread should
		# be stopped
		self.stopped = False

	def start(self):
		# start the thread to read frames from the video stream
		Thread(target=self.update, args=()).start()
		return self

	def update(self):
		# keep looping infinitely until the thread is stopped
		while True:
			# if the thread indicator variable is set, stop the thread
			if self.stopped:
				return

			while True:
				try:
					(self.grabbed, self.frame) = self.stream.read()
					break
				except:
					continue

	def read(self):
		# return the frame most recently read
		return self.frame

	def stop(self):
		# indicate that the thread should be stopped
		self.stopped = True

print("[INFO] sampling frames from webcam...")
cap = cv2.VideoCapture(0, cv2.CAP_V4L2)
#cap.set(cv2.CAP_PROP_FPS, 11)
#cap.set(cv2.CAP_PROP_AUTOFOCUS, 1)
cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 4280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 3120)
cap.set(cv2.CAP_PROP_BUFFERSIZE, 1);
fps = FPS().start()
 
# loop over some frames
while fps._numFrames < 50:
	# grab the frame from the stream and resize it to have a maximum
	# width of 400 pixels
	
	(grabbed, frame) = cap.read()

	# update the FPS counter
	fps.update()
 
# stop the timer and display FPS information
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
 
# do a bit of cleanup
cap.release()
cv2.destroyAllWindows()

print("[INFO] sampling THREADED frames from webcam...")
vs = WebcamVideoStream(src=0).start()
fps = FPS().start()
 
# loop over some frames...this time using the threaded stream
while fps._numFrames < 50:
	# grab the frame from the threaded video stream and resize it
	# to have a maximum width of 400 pixels
	frame = vs.read()
 
	# check to see if the frame should be displayed to our screen
	cv2.imshow("hhk", frame)
	c = cv2.waitKey(1)

	if c & 0xFF == ord('g'):
		break
	# update the FPS counter
	fps.update()
 
# stop the timer and display FPS information
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
 
# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()