#!/usr/bin/env python
import PySimpleGUI as sg
# import PySimpleGUIQt as sg
import cv2
import numpy as np
import sys
from sys import exit as exit
import time
"""
Demo program that displays a webcam using OpenCV
"""
def de(window):
    window['output1'].Update("De")
    window.Refresh()
    time.sleep(3)
    window['output2'].Update("Takoy1")
    window.Refresh()



def main():

    sg.ChangeLookAndFeel('Black')

    # define the window layout
    layout = [[sg.Text('', size=(50,1), key='output1'),sg.Text('', size=(50,1), key='output2'), sg.Button('Execute'), sg.Button('Exit')]]

    # create the window and show it without the plot
    window = sg.Window('Demo Application - OpenCV Integration', layout,
                       location=(800,400))

    # ---===--- Event LOOP Read and display frames, operate the GUI --- #
    i = 0
    while True:
        event, values = window.Read(timeout=20)
        if event is None or event == 'Exit':
            break
        elif event == 'Execute':
            de(window)


main()
exit()