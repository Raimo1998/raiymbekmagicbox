from google.cloud import vision
from google.cloud.vision import types
from gcloud import storage
import cv2
import timeit
import Levenshtein
import csv
start = timeit.default_timer()
storage_client = storage.Client()
bucket = storage_client.get_bucket('magic_box_images')
blob = bucket.blob('forOCR.jpg')

with open('de.jpg', 'rb') as photo:
    blob.upload_from_file(photo)

image_uri = 'gs://magic_box_images/forOCR.jpg'
client = vision.ImageAnnotatorClient()
image = types.Image()
image.source.image_uri = image_uri

response = client.text_detection(image=image)

prob_name_list = []
prob_last_list = []
word_list = response.text_annotations[1:]
i = 0
while i < len(word_list) -1:
    if word_list[i].description.isupper():
        if word_list[i+1].description.isupper():
            prob_name_list.append(word_list[i].description)
            prob_last_list.append(word_list[i+1].description)
            i = i + 1
        else:
            i = i + 2
    else:
        i = i+1

def get_all_user_list():
    name_list = []
    last_list = []
    with open("UserFirstandLastNames.csv") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            name_list.append(row[0])
            last_list.append(row[1])
    return name_list, last_list

def get_recipient_name(name_list, last_list ,prob_name_list, prob_last_list):
    recipient = []
    for name_ind in range(len(name_list)):
        for prob_name_ind in range(len(prob_name_list)):
            if name_list[name_ind] != "":
                if Levenshtein.ratio(name_list[name_ind].upper(), prob_name_list[prob_name_ind]) >= 0.95 - (1 / max(len(name_list[name_ind]),len(prob_name_list[prob_name_ind]))):
                    if Levenshtein.ratio(last_list[name_ind].upper(), prob_last_list[prob_name_ind]) >= 0.95 - (1 / max(len(last_list[name_ind]),len(prob_last_list[prob_name_ind]))):
                        toAdd = name_list[name_ind] + " " + last_list[name_ind]
                        if not(toAdd in recipient):
                            recipient.append(toAdd) 
    if len(recipient) == 1:
        print("The recipient " + recipient[0])
        return True, recipient[0]
    else:
        print("Possible recipients: ")
        print(recipient)
        return False, recipient 

name_list, last_list = get_all_user_list()
one_rec, recipient = get_recipient_name(name_list, last_list ,prob_name_list, prob_last_list)
stop = timeit.default_timer()
print('Time: ', stop - start)