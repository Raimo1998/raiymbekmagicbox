#import execute
#import cameraPlayground
import pyrealsense2 as rs
import numpy as np
import cv2
import time 
import timeit
from google.cloud import vision
from google.cloud.vision import types
from gcloud import storage
from matplotlib import pyplot as plt
import Levenshtein
import csv
import os
from threading import Thread
import datetime
import PySimpleGUI as sg

vs = cv2.VideoCapture(0)
vs.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M','J','P','G')) #cv2.VideoWriter_fourcc(*'MJPG'))
vs.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)
vs.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
vs.set(cv2.CAP_PROP_BUFFERSIZE, 1);

while True:
    ret, frame = vs.read()
    center_img = frame[10 : 2155, 450 : 2500]
    text = "Bluryness"
    fm = cv2.Laplacian(center_img, cv2.CV_64F).var()
    print("Clariness: ")
    print(fm)
    print("\n")
    cv2.putText( frame, "{}: {:.2f}".format(text, fm), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
    box = np.array([[450, 10], [2510, 10],[2510, 2155],[450, 2155]])
    contour_img = cv2.drawContours(frame.copy(),[box],0,(0,0,255),3)
    cv2.imshow('Input', cv2.resize( contour_img,(int(frame.shape[1]/3), int(frame.shape[0]/3))))

    key = cv2.waitKey(1)
        # Press esc or 'q' to close the image window
    if key & 0xFF == ord('q') or key == 27:
        cv2.destroyAllWindows()
        break

vs.release()
