import pyrealsense2 as rs
import numpy as np
import cv2
import time 
import timeit
from google.cloud import vision
from google.cloud.vision import types
from gcloud import storage
from matplotlib import pyplot as plt
import Levenshtein
import csv
import os
from threading import Thread
import datetime
import PySimpleGUI as sg
import serial


class Execute:
    def __init__(self, window):
        # PySimple GUi helpers
        self.toDisplay = '\n\n\n-------------\n'
        self.selfwindow = window
        # Configuring depth camera
        self.configure_streams()

        #Height without a package
        self.totalHeight = self.depth_now()
        self.baseWeight = self.weight_now()

        # Getting the list of recipients 
        self.get_all_user_list()
        print(self.totalHeight)

        #Boundaries for the magic box base 
        self.upBound = 10
        self.downBound = 2155
        self.leftBound = 450
        self.rightBound = 2500


    def depth_now(self):
        #Return average height for the next five frames
        if self.depthon:
            depth = 0
            i = 0
            while i < 5:
                frames = self.pipeline.wait_for_frames()
                depth_frame = frames.get_depth_frame()
                if 0 >= depth_frame.get_distance(249, 124):
                    continue
                depth += depth_frame.get_distance(249, 124)
                i+=1
            return depth / 5
        return 0

    def weight_now(self):
        #Return average height for the next five frames
        i = 0
        weight = float(0)
        while i < 3:
            self.ser.write(('W\r').encode('utf-8')) #command to read the weight
            self.ser.flush()
            out = ''
            time.sleep(0.1)
            while self.ser.inWaiting() > 0:
                out +=self.ser.read(1).decode("utf-8")
            weight += float(out)
            i+=1
        return weight / 3

    def configure_streams(self):
        # Setting depth camera
        try:
            self.pipeline = rs.pipeline()
            config = rs.config()
            config.enable_stream(rs.stream.depth)
            self.pipeline.start(config)
            self.depthon = True
        except:
            self.depthon = False
        
        # Setting Normal Camera 
        self.vs = cv2.VideoCapture(0, cv2.CAP_V4L2)
        self.vs.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M','J','P','G')) #cv2.VideoWriter_fourcc(*'MJPG'))
        self.vs.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)
        self.vs.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
        self.vs.set(cv2.CAP_PROP_BUFFERSIZE, 1)

        port='/dev/ttyUSB0'
        if port == 'FILLOUTPORT':
          port = input("Please enter a port: ")
        try:
          self.ser = serial.Serial(port=port)
          print("Opening Port {}".format(port))
          if not self.ser.isOpen:
            self.ser.open()
          print("Opened Port {}".format(port))
        except Exception as e:
          print("Could not open Port {}".format(port))
          print(e)
          self.ser = None
        

    def detect_package(self):
        # Infine loop looking for the height changes to detect a package and its hight

        # variable for the current height
        current = self.totalHeight

        i = 0

        while True:

            print(current)
            #Getting current depth
            frames = self.pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            depth = depth_frame.get_distance(249, 124)

            # detect depth changes
            if depth != 0 and abs(current - depth) > 0.02:
                i = 0
                current = depth
                print("dd")
            else:
                i+=1
                current = depth

            # If a new depth stays for 50 consequent frames 
            if i == 50 and abs(current - self.totalHeight) > 0.02:
                #self.pipeline.stop()
                break
            
        return self.totalHeight - current, current, self.weight_now() - self.baseWeight

    def get_img(self, img_path):
        # Given path get the img
        img = cv2.imread(img_path)
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        return img

    def print_img(self, img):
        # just prints an image
        plt.imshow(img, cmap='Greys_r')
        plt.show()

    def get_box_img(self, img):

        # Resize image from normal camera
        smaller_img = cv2.resize(img, (int(img.shape[1]/8), int(img.shape[0]/8)))

        #Getting edges of the img from Laplacian Gaussian filter
        blur = cv2.GaussianBlur(smaller_img,(11,11),0)                    
        lap = cv2.Laplacian(blur, cv2.CV_32F)                                
        grey = (cv2.cvtColor(lap, cv2.COLOR_BGR2GRAY) + 120).astype(np.uint8)
        edges = cv2.Canny(grey, 50, 150, apertureSize = 5)
        #edges = hist_eq(edges)

        #Getting all contours 
        contours, hierarchy = cv2.findContours(edges, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        contour_img = img.copy()
        #print_img(cv2.drawContours(contour_img,[contours],-1,(0,0,255),3))

        # Get all big contours and unite all of it
        finalContour = np.array([[[0,0]]])
        for cnt in contours:
            cnt = cv2.convexHull(cnt)
            if cv2.contourArea(cnt) > 500:
                finalContour = np.vstack((finalContour, cnt))
        finalContour = cv2.convexHull(finalContour[1:])

        # Getting retangle from the contour
        rect = cv2.minAreaRect(finalContour)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        box = 8 * box

        contour_img = cv2.drawContours(contour_img,[box],0,(0,0,255),3)

        return contour_img, box

    def get_google_text(self, img):
        # Getting OCR results from google ocr service
        storage_client = storage.Client()
        bucket = storage_client.get_bucket('magic_box_images')
        blob = bucket.blob('forOCR.jpg')

        with open('de.jpg', 'rb') as photo:
            blob.upload_from_file(photo)

        image_uri = 'gs://magic_box_images/forOCR.jpg'
        client = vision.ImageAnnotatorClient()
        image = types.Image()
        image.source.image_uri = image_uri

        response = client.text_detection(image=image)

        prob_name_list = []
        prob_last_list = []
        word_list = response.text_annotations[1:]
        text = response.text_annotations[0].description
        i = 0

        # getting probable first name ad last name list 
        while i < len(word_list) -1:
            # If word are upper case add them
            if word_list[i].description.isupper():
                if word_list[i+1].description.isupper():
                    prob_name_list.append(word_list[i].description)
                    prob_last_list.append(word_list[i+1].description)
                    i = i + 1
                else:
                    i = i + 2
            else:
                i = i+1
        return text,prob_name_list, prob_last_list

    def get_package(self, img, box, current):
        # having box coordinates and img get the slice of the box
        box = np.float32(box)

        # Length and Width of the box
        length, width = self.get_size(box)
        print("Pixel size", length, width)

        # Converting from pixel length to the actual length and width using the heigth
        cm_length = (current / self.totalHeight) * (30.5 / 1280) * length
        cm_width = (current / self.totalHeight) * (30.5 / 1280) * width

        # perspective transform 
        next_box = np.float32([[0, length], [0,0], [width,0], [width, length]])
        M = cv2.getPerspectiveTransform(box, next_box)
        return cv2.warpPerspective(img, M, (width, length)), cm_length, cm_width

    def get_size(self, box):
        # Getting pixel size of the box
        first, second, third, fourth = box
        length = int(( (first[0] - second[0])**2 + (first[1] - second[1])**2 )**0.5)
        width = int(( (third[0] - second[0])**2 + (third[1] - second[1])**2 )**0.5)
        return length, width

    def get_img_th(self):
        # Get the photo of the base

        #vs = WebcamVideoStream(src=0).start()
        ret, frame = self.vs.read()
        ret, frame = self.vs.read()
        center_img = frame[self.upBound : self.downBound, self.leftBound : self.rightBound]
        
        # This code is for previous autofocus camera whne it waiting for the photo to be clear
        """
        while True:
            # grab the frame from the threaded video stream and resize it
            # to have a maximum width of 400 pixels
            ret, frame = self.vs.read()
            center_img = frame[20 : 2155, 390 : 3590]
            print(cv2.Laplacian(center_img, cv2.CV_64F).var())
            if cv2.Laplacian(center_img, cv2.CV_64F).var() > 50:
                #self.vs.stop()
                #self.vs.release()
                break
            #self.print_img(center_img)
        """
        return center_img

    def get_all_user_list(self):
        # Getting first and last name list from a csv file
        self.name_list = []
        self.last_list = []
        with open("UserFirstandLastNames.csv") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                self.name_list.append(row[0])
                self.last_list.append(row[1])

    def get_recipient_name(self,prob_name_list, prob_last_list):
        # Having all the recipient first and last name list go over probable list and get similarities
        recipient = []

        # Looping over recipient names
        for name_ind in range(len(self.name_list)):
            # Looping over all the probable names
            for prob_name_ind in range(len(prob_name_list)):
                if self.name_list[name_ind] != "":
                    # Using Levenshtein to compare names but the threshold is dependent on the length of the name
                    if Levenshtein.ratio(self.name_list[name_ind].upper(), prob_name_list[prob_name_ind]) >= 0.85 - (1 / max(len(self.name_list[name_ind]),len(prob_name_list[prob_name_ind]))):
                        # Using Levenshtein to compare last names
                        if Levenshtein.ratio(self.last_list[name_ind].upper(), prob_last_list[prob_name_ind]) >= 0.85 - (1 / max(len(self.last_list[name_ind]),len(prob_last_list[prob_name_ind]))):
                            # If recipient name and last name match add to all matches
                            toAdd = self.name_list[name_ind] + " " + self.last_list[name_ind]
                            if not(toAdd in recipient):
                                recipient.append(toAdd) 
        return recipient 

    def show_preview(self):
        # Showing the preview 
        ret, frame = self.vs.read()
        center_img = frame[self.upBound : self.downBound, self.leftBound : self.rightBound]
        
        # Laplacian to check image for clarity
        fm = cv2.Laplacian(center_img, cv2.CV_64F).var()
        text = "Clariness"
        print("Clariness: ")
        print(fm)
        print("\n")
        cv2.putText( frame, "{}: {:.2f}".format(text, fm), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
        box = np.array([[self.leftBound, self.upBound], [self.rightBound, self.upBound],[self.rightBound, self.downBound],[self.leftBound, self.downBound]])
        contour_img = cv2.drawContours(frame.copy(),[box],0,(0,0,255),3)
        imgbytes=cv2.imencode('.png', cv2.resize( contour_img,(800,500)).astype(np.uint8))[1].tobytes()
        #return imgbytes
        self.selfwindow.FindElement('image').Update(data=imgbytes)
        #self.selfwindow['-OUTPUT-'].Update(str(fm))
        return fm
        #cv2.imshow('Input', cv2.resize( contour_img,(int(frame.shape[1]/3), int(frame.shape[0]/3))))

    def restart(self):
        # Restarting the depth frame waiting utill person will take the package
        i = 0

        while True:
            frames = self.pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            depth = depth_frame.get_distance(249, 124)

            if depth != 0 and abs(self.totalHeight - depth) < 0.02:
                i += 1
            else:
                i = 0

            if i == 20:
                break

    def get_focus(self):
        # Gettign focus of the image
        ret, frame = self.vs.read()
        ret, frame = self.vs.read()
        center_img = frame[10 : 2155, 250 : 3440]
        return cv2.Laplacian(center_img, cv2.CV_64F).var()

    def calibrate(self):
        # Calibaration in case camera goes out of focus

        # if depth camera is not on wait untill it gets plugged in
        if not self.depthon:
            self.selfwindow['-OUTPUT-'].Update("Replug the depth camera")
            self.selfwindow.Refresh()
            while True:
                try:
                    self.pipeline = rs.pipeline()
                    config = rs.config()
                    config.enable_stream(rs.stream.depth)
                    self.pipeline.start(config)
                    self.depthon = True
                    self.totalHeight = self.depth_now()
                    self.selfwindow['-OUTPUT-'].Update("depth camera pluged")
                    self.selfwindow.Refresh()
                    break
                except:
                    print("de")
        self.totalHeight = self.depth_now()
        self.baseWeight = self.weight_now()
        self.selfwindow['-OUTPUT-'].Update("depth camera pluged")
        self.selfwindow.Refresh()

    def execute(self):
        print("starting")

        #------------------------------------------------

        # Package Detection
        self.selfwindow['-OUTPUT-'].Update("Place a package")
        self.selfwindow.Refresh()

        boxHeight, current, boxWeight = self.detect_package()

        self.selfwindow['-OUTPUT-'].Update("Package detected")
        self.selfwindow.Refresh()
        # detected

        #------------------------------------------------

        # Taking the image 
        image = self.get_img_th()
        # Finished taking Image

        #------------------------------------------------

        # Calculating Dimentions
        try:
            cv2.imwrite("picture.jpg", image)
            contour_img, box = self.get_box_img(image)
            package, cm_length, cm_width = self.get_package(image, box, current)
            cv2.imwrite("de.jpg", package)
            toReturn = "Dimentions: " 
            toReturn += str(boxHeight * 100)[0:5] + " X "
            toReturn += str(cm_length)[0:5] + " X "
            toReturn += str(cm_width)[0:5]
            fit_package = package
            if cm_length > cm_width:
                fit_package = cv2.rotate(package, cv2.ROTATE_90_CLOCKWISE)

            imgbytes2=cv2.imencode('.png', cv2.resize( fit_package,(800, 500)).astype(np.uint8))[1].tobytes()
            self.selfwindow['-OUTPUT-'].Update(toReturn)
            self.selfwindow.FindElement('image').Update(data=imgbytes2)
            self.selfwindow.Refresh()
            self.message = toReturn
        except:
            self.selfwindow['-OUTPUT-'].Update("Make sure the camera was calibrated and you placed a package. Try again")
            self.selfwindow.Refresh()
            return False
        # Finished dimentions

        #-------------------------------------------------

        # Getting Text
        text, prob_name_list, prob_last_list = self.get_google_text(package)
        recipient = self.get_recipient_name(prob_name_list, prob_last_list)
        cv2.imwrite("forOCR.jpg", contour_img)

        if len(recipient) == 1:
            toReturn = "Possible recipients are: " + str(recipient)
            self.selfwindow['-OUTPUT-'].Update(toReturn)
            self.message = toReturn + "\n" + self.message + " (cm)" + "\nweight: " + str(round(boxWeight,2)) + " (lb)" + "\n-------------\n"
            self.toDisplay = self.toDisplay + self.message
            self.selfwindow['result'].Update(self.toDisplay)
            self.selfwindow.Refresh()
        else:
            toReturn = "Possible recipients are: " + str(recipient)
            self.selfwindow['-OUTPUT-'].Update(toReturn)
            self.message = toReturn + "\n" + self.message + " (cm)" + "\nweight: " + str(round(boxWeight,2)) + " (lb)" + "\n-------------\n"
            self.toDisplay = self.toDisplay + self.message
            self.selfwindow['result'].Update(self.toDisplay)
            self.selfwindow.Refresh()
        # finished with text

        #-------------------------------------------------

        # Saving the data
        now = datetime.datetime.now()
        string_now = now.strftime("%Y-%m-%d %H:%M")
        cv2.imwrite("packages/" + string_now + ".jpg", package)
        file1 = open("text/" + string_now +".txt","w")
        file1.write(text)
        file1.close()
        # finishing the save
        cv2.destroyAllWindows()

        #-------------------------------------------------

        #Restarting the process
        self.selfwindow['-OUTPUT-'].Update("Take package")
        self.selfwindow.Refresh()
        self.restart()
        return True

class WebcamVideoStream:
    # Class for using multithreading for preview but actually did not increase performance a lot
    def __init__(self, src=0):
        # initialize the video camera stream and read the first frame
        # from the stream
        self.stream = cv2.VideoCapture(src, cv2.CAP_V4L2)
        #self.stream.set(cv2.CAP_PROP_AUTOFOCUS, 1)
        self.stream.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
        self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)
        self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
        while True:
            try:
                (self.grabbed, self.frame) = self.stream.read()
                break
            except:
                continue

        # initialize the variable used to indicate if the thread should
        # be stopped
        self.stopped = False

    def start(self):
        # start the thread to read frames from the video stream
        Thread(target=self.update, args=()).start()
        return self

    def update(self):
        # keep looping infinitely until the thread is stopped
        while True:
            # if the thread indicator variable is set, stop the thread
            if self.stopped:
                return

            while True:
                try:
                    (self.grabbed, self.frame) = self.stream.read()
                    break
                except:
                    continue

    def read(self):
        # return the frame most recently read
        return self.frame

    def stop(self):
        # indicate that the thread should be stopped
        self.stopped = True
